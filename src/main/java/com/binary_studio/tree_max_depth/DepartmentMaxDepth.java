package com.binary_studio.tree_max_depth;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		int depth = 0;
		Queue<Department> queue = new LinkedList<>();
		if (rootDepartment != null) {
			queue.add(rootDepartment);
		}
		while (!queue.isEmpty()) {
			var queueCount = queue.size();
			while (queueCount > 0) {
				queue.poll().getSubDepartments().stream().filter(Objects::nonNull).forEach(queue::add);
				queueCount--;
			}
			depth += 1;
		}
		return depth;
	}

}
