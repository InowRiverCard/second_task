package com.binary_studio.uniq_in_sorted_stream;

import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		Counter counter = new UniqueSortedStream().new Counter();
		return stream.filter(row -> {
			if (row.getPrimaryId() == counter.getPointer()) {
				return false;
			}
			else {
				counter.setPointer(row.getPrimaryId());
				return true;
			}
		});
	}

	private class Counter {

		private long pointer = -1;

		long getPointer() {
			return this.pointer;
		}

		void setPointer(long pointer) {
			this.pointer = pointer;
		}

	}

}
