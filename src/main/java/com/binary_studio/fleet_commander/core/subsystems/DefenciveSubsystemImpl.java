package com.binary_studio.fleet_commander.core.subsystems;

import java.util.logging.Logger;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.util.ClassNameUtil;

/**
 * The class implements ship defence functionality such as regeneration and reducing
 * impact damage.
 *
 * @author Aleksandr Karpachov
 * @version 1.0
 */
public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	/**
	 * Logger
	 */
	private static final Logger LOG = Logger.getLogger(ClassNameUtil.getCurrentClassName());

	/**
	 * Maximum percentage by which we can reduce incoming damage.
	 */
	private final static int MAX_IMPACT_REDUCTION = 95;

	/**
	 * Name of the system
	 */
	private String name;

	/**
	 * Percentage by which current system reduces incoming damage.
	 */
	private PositiveInteger impactReduction;

	/**
	 * Amount of EHP by which the current defencive system can restore power shields.
	 */
	private PositiveInteger shieldRegen;

	/**
	 * Amount of EHP by which the current defencive system can restore hull.
	 */
	private PositiveInteger hullRegen;

	/**
	 * Amount of GA/H which the defencive system requires for one cycle of work.
	 */
	private PositiveInteger capacitorConsumption;

	/**
	 * Amount of MW which the defencive system requires for installing.
	 */
	private PositiveInteger powerGridConsumption;

	private DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		if (name == null || name.isBlank()) {
			LOG.fine("Attempt to create defensive system with null of blank name");
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.impactReduction = PositiveInteger.of(Math.min(impactReduction.value(), MAX_IMPACT_REDUCTION));
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorConsumption = capacitorUsage;
		this.powerGridConsumption = pgRequirement;
	}

	/**
	 * Fabric method for creating instance of current class.
	 * @return new instance of this class.
	 * @throws IllegalArgumentException if name is empty or null.
	 */
	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		return new DefenciveSubsystemImpl(name, impactReductionPercent, shieldRegeneration, hullRegeneration,
				capacitorConsumption, powergridConsumption);
	}

	/**
	 * @return amount of MW which the current defencive system requires for installing.
	 */
	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.powerGridConsumption;
	}

	/**
	 * @return amount of GA/H which the defencive system requires for one cycle of work.
	 */
	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorConsumption;
	}

	/**
	 * @return name of the current system.
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * The method calculates how mach damage penetrates through the current defensive
	 * system. Formula for calculating: damage = incoming damage * (1 - (impact reduction
	 * / 100)). Damage always rounded to bigger integer.
	 * @param incomingDamage {@link AttackAction} with amount of damage from the attacker.
	 * @return {@link AttackAction} with amount of damage which penetrates through the
	 * system.
	 */
	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		double penetrationRate = 1 - (double) this.impactReduction.value() / 100;
		double preciseDamage = incomingDamage.damage.value() * penetrationRate;
		int damage = (int) Math.ceil(preciseDamage);
		LOG.fine(String.format("Reduce damage. Reduction rate=%s, income damage=%s, passed damage =%s",
				this.impactReduction, incomingDamage.damage, damage));
		return new AttackAction(PositiveInteger.of(damage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	/**
	 * @return {@link RegenerateAction} with amount of EHP by which the system restores
	 * Hull and Shields.
	 */
	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

	@Override
	public String toString() {
		return String.format(
				"DefenciveSubsystemImpl={name=%s, impactReduction=%s, shieldRegen=%s, hullRegen=%s, "
						+ "capacitorConsumption=%s, powerGridConsumption=%s}",
				this.name, this.impactReduction, this.shieldRegen, this.hullRegen, this.capacitorConsumption,
				this.powerGridConsumption);
	}

}
