package com.binary_studio.fleet_commander.core.subsystems;

import java.util.logging.Logger;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.util.ClassNameUtil;

/**
 * The class implements ship attack functionality
 *
 * @author Aleksandr Karpachov
 * @version 1.0
 */
public final class AttackSubsystemImpl implements AttackSubsystem {

	/**
	 * Logger
	 */
	private static final Logger LOG = Logger.getLogger(ClassNameUtil.getCurrentClassName());

	/**
	 * Systems name
	 */
	private String name;

	/**
	 * Base amount of damage the attack system can do
	 */
	private PositiveInteger baseDamage;

	/**
	 * Minimum size of an attacked object for maximum damage. Measured in meters.
	 */
	private PositiveInteger optimalSize;

	/**
	 * Maximum speed of an attacked object for maximum damage. Measured in m/s.
	 */
	private PositiveInteger optimalSpeed;

	/**
	 * Amount of GA / H required by the attacking system for one shot.
	 */
	private PositiveInteger capacitorUsage;

	/**
	 * Amount of MW required by the attacking system for installing.
	 */
	private PositiveInteger pgRequirement;

	private AttackSubsystemImpl(String name, PositiveInteger baseDamage, PositiveInteger optimalSize,
			PositiveInteger optimalSpeed, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		if (name == null || name.isBlank()) {
			LOG.fine("Attempt to create defensive system with null of blank name");
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	/**
	 * Fabric method for creating new instance of an attack system.
	 * @return new instance of this class.
	 * @throws IllegalArgumentException if name is empty or null.
	 */
	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirements,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		return new AttackSubsystemImpl(name, baseDamage, optimalSize, optimalSpeed, capacitorConsumption,
				powergridRequirements);
	}

	/**
	 * @return amount of MW which the current attack system requires for installing.
	 */
	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	/**
	 * @return amount of GA/H which the attack system requires for one cycle of work.
	 */
	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	/**
	 * The method calculates how much damage this system can inflict on the target.
	 * Formula for calculating: sizeReductionModifier = when targetSize >= optimalSize ->
	 * 1 else targetSize / optimalSize speedReductionModifier = when targetSpeed <=
	 * optimalSpeed -> 1 else optimalSpeed / (2 * targetSpeed) damage = baseDamage *
	 * min(sizeReductionModifier, speedReductionModifier) damage always rounded to bigger
	 * integer.
	 * @param target {@link Attackable} which contain speed and size of the target.
	 * @return {@link PositiveInteger} with number of damage.
	 */
	@Override
	public PositiveInteger attack(Attackable target) {
		int result = 0;
		LOG.fine("attack target " + target.toString());
		double sizeModifier = getSizeReductionModifier(target.getSize().value());
		double speedModifier = getSpeedReductionModifier(target.getCurrentSpeed().value());
		result = (int) Math.ceil(this.baseDamage.value() * Math.min(sizeModifier, speedModifier));
		LOG.fine("calculated damage = " + result);
		return PositiveInteger.of(result);
	}

	/**
	 * The method calculate size reduction modifier Formula for calculating:
	 * sizeReductionModifier = when targetSize >= optimalSize -> 1 else targetSize /
	 * optimalSize
	 * @param objectSize attacking object size
	 * @return size reduction modifier which always >= 1.
	 */
	private double getSizeReductionModifier(int objectSize) {
		double result;
		var optimalSize = this.optimalSize.value();
		if (objectSize >= optimalSize) {
			result = 1;
		}
		else {
			result = (double) objectSize / optimalSize;
		}
		return result;
	}

	/**
	 * The method calculate speed reduction modifier speedReductionModifier = when
	 * targetSpeed <= optimalSpeed -> 1 * else optimalSpeed / (2 * targetSpeed)
	 * @param objectSpeed attacking object size
	 * @return speed eduction modifier which always >= 1.
	 */
	private double getSpeedReductionModifier(int objectSpeed) {
		double result;
		var optimalSpeed = this.optimalSpeed.value();
		if (objectSpeed <= optimalSpeed) {
			result = 1;
		}
		else {
			result = (double) optimalSpeed / (objectSpeed * 2);
		}
		return result;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return String.format(
				"AttackSubsystemImpl={name=%s, baseDamage=%s, optimalSize=%s, optimalSpeed=%s, capacitorUsage=%s,"
						+ " pgRequirement=%s}",
				this.name, this.baseDamage, this.optimalSize, this.optimalSpeed, this.capacitorUsage,
				this.pgRequirement);
	}

}
