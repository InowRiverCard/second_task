package com.binary_studio.fleet_commander.core.common;

/**
 * Common interface for objects in game.
 */
public interface NamedEntity {

	String getName();

}
