package com.binary_studio.fleet_commander.core.common;

/**
 * Interface for objects or ships which can be attacked.
 */
public interface Attackable extends NamedEntity {

	PositiveInteger getSize();

	PositiveInteger getCurrentSpeed();

}
