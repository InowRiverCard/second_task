package com.binary_studio.fleet_commander.core.ship;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.SubsystemKey;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;
import com.binary_studio.fleet_commander.util.ClassNameUtil;

/**
 * The class describes docked ship. It includes functionality for ship fitting and
 * acquisition of an udocked ship
 *
 * @author Aleksandr Karpachov
 * @version 1.0
 */
public final class DockedShip implements ModularVessel {

	/**
	 * Logger
	 */
	private static final Logger LOG = Logger.getLogger(ClassNameUtil.getCurrentClassName());

	/**
	 * Systems name
	 */
	private String name;

	/**
	 * Ship shields <em>EHP</em>
	 */
	private PositiveInteger shieldHP;

	/**
	 * Ship hull <em>EHP</em>
	 */
	private PositiveInteger hullHP;

	/**
	 * Ship capacitor capacity <em>sGA/H</em>.
	 */
	private PositiveInteger capacitor;

	/**
	 * Ship capacitor regeneration per turn
	 */
	private PositiveInteger capacitorRegeneration;

	/**
	 * Ship powergrid capacity <em>MW</em>.
	 */
	private PositiveInteger powergrid;

	/**
	 * Map contain all ship subsystems.
	 */
	private final Map<SubsystemKey, Subsystem> subsystems;

	/**
	 * Ship speed <em>m/s</em>.
	 */
	private PositiveInteger speed;

	/**
	 * Ship size <em>m</em>.
	 */
	private PositiveInteger size;

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger powergrid, PositiveInteger speed,
			PositiveInteger size) {
		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.powergrid = powergrid;
		this.speed = speed;
		this.size = size;
		this.subsystems = new HashMap<>();
	}

	/**
	 * Fabric method for creating new instance of this class.
	 * @return new instance of {@link DockedShip}.
	 * @throws IllegalArgumentException if name is empty or null.
	 */
	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, powergridOutput, speed,
				size);
	}

	/**
	 * Method for installing, removing or reinstalling an attack system. In case of
	 * unsuccessful re-fit, the old subsystem remains.
	 * @param subsystem {@link AttackSubsystem} or null for deinstalling.
	 * @throws InsufficientPowergridException if you tries to install system which consume
	 * more powergrid then is left on the ship.
	 */
	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		Subsystem currentSubsystem = this.subsystems.get(SubsystemKey.ATTACK);
		if (subsystem != null && currentSubsystem == null) {
			this.fitSubsystem(subsystem, SubsystemKey.ATTACK);
		}
		else if (currentSubsystem != null && subsystem == null) {
			this.removeSubsystem(SubsystemKey.ATTACK);
		}
		else if (currentSubsystem != null) {
			this.refitSubsystem(subsystem, SubsystemKey.ATTACK);
		}
		else {
			LOG.fine(String.format("Ship \"%s\". Attempt to remove a missing(null) attack system", this.name));
		}
	}

	/**
	 * Method for installing, removing and reinstalling a defencive system. In case of
	 * unsuccessful re-fit, the old subsystem remains.
	 * @param subsystem {@link DefenciveSubsystem} or null for deinstalling.
	 * @throws InsufficientPowergridException if you tries to install system which
	 * consumes. more powergrid then is left on the ship.
	 */
	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		Subsystem currentSubsystem = this.subsystems.get(SubsystemKey.DEFENCIVE);
		if (subsystem != null && currentSubsystem == null) {
			this.fitSubsystem(subsystem, SubsystemKey.DEFENCIVE);
		}
		else if (currentSubsystem != null && subsystem == null) {
			this.removeSubsystem(SubsystemKey.DEFENCIVE);
		}
		else if (currentSubsystem != null) {
			this.refitSubsystem(subsystem, SubsystemKey.DEFENCIVE);
		}
		else {
			LOG.fine(String.format("Ship \"%s\". Attempt to remove a missing(null) defensive system", this.name));
		}
	}

	/**
	 * Method for installing subsystems.
	 * @param subsystem for install.
	 * @param type of subsystem to install.
	 * @throws InsufficientPowergridException if you tries to install system which
	 * consumes. more powergrid then is left on the ship.
	 */
	private void fitSubsystem(Subsystem subsystem, SubsystemKey type) throws InsufficientPowergridException {
		LOG.fine(String.format("Ship \"%s\". Attempt to set %s", subsystem.toString(), this.name));
		this.reducePG(subsystem.getPowerGridConsumption());
		this.subsystems.put(type, subsystem);
		LOG.fine(String.format("Ship \"%s\". Subsystem successfully installed, current powergrid=%d", this.name,
				this.powergrid.value()));
	}

	/**
	 * Method for removing subsystems.
	 * @param type of subsystem to remove.
	 */
	private void removeSubsystem(SubsystemKey type) {
		Subsystem subsystem = this.subsystems.get(type);
		this.releasePG(subsystem.getPowerGridConsumption());
		this.subsystems.remove(type);
		LOG.fine(String.format("Ship \"%s\". System \"%s\" was removed, current powergrid = %d", this.name, subsystem,
				this.powergrid.value()));
	}

	/**
	 * Method for reinstalling subsystems.
	 * @param subsystem for install.
	 * @param type of subsystem to install.
	 * @throws InsufficientPowergridException if you tries to install system which
	 * consumes. more powergrid then is left on the ship.
	 */
	private void refitSubsystem(Subsystem subsystem, SubsystemKey type) throws InsufficientPowergridException {
		Subsystem old = this.subsystems.get(type);
		this.releasePG(old.getPowerGridConsumption());
		try {
			this.reducePG(subsystem.getPowerGridConsumption());
			this.subsystems.put(type, subsystem);
			LOG.fine(String.format("Ship \"%s\". \"%s\" was changed to \"%s\"", this.name, old.toString(),
					subsystem.toString()));
		}
		catch (InsufficientPowergridException e) {
			this.reducePG(old.getPowerGridConsumption());
			LOG.fine(String.format("Ship \"%s\". Attempt to change \"%s\" to \"%s\" failed", this.name, old.toString(),
					subsystem.toString()));
			throw e;
		}
	}

	/**
	 * Method for reducing pg when you install subsystems.
	 * @param consumption amount of PG used by the subsystem.
	 */
	private void reducePG(PositiveInteger consumption) throws InsufficientPowergridException {
		int pg = this.powergrid.value();
		if (consumption.value() > pg) {
			int insufficientPG = pg - consumption.value();
			LOG.fine(String.format(
					"Сan’t install the subsystem due to lack of powergrid," + " current pg=%d, necessary=%d", pg,
					consumption.value()));
			throw new InsufficientPowergridException(insufficientPG);
		}
		this.powergrid = PositiveInteger.of(pg - consumption.value());
	}

	/**
	 * Methods for adding PG when you remove subsystem.
	 * @param value amount of releasing pg <em>MW</em>;
	 */
	private void releasePG(PositiveInteger value) {
		int updatedPG = value.value() + this.powergrid.value();
		this.powergrid = PositiveInteger.of(updatedPG);
	}

	/**
	 * Method for obtaining undocked {@link CombatReadyShip} ship based on current ship.
	 * @return new instance of {@link CombatReadyShip}
	 * @throws NotAllSubsystemsFitted when you tries to invoke this method before
	 * installing attack and defencive subsystems.
	 */
	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		var attackSubsystem = (AttackSubsystem) this.subsystems.get(SubsystemKey.ATTACK);
		var defenciveSubsystem = (DefenciveSubsystem) this.subsystems.get(SubsystemKey.DEFENCIVE);
		if (attackSubsystem == null && defenciveSubsystem == null) {
			LOG.fine(String.format("Attempt to undock ship \"%s\" without attack and defence systems", this.name));
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (attackSubsystem == null) {
			LOG.fine(String.format("Attempt to undock ship \"%s\" without attack system", this.name));
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (defenciveSubsystem == null) {
			LOG.fine(String.format("Attempt to undock ship \"%s\" without attack system", this.name));
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		LOG.fine(String.format("Ship \"%s\" left dock", this.name));
		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitor, this.capacitorRegeneration,
				attackSubsystem, defenciveSubsystem, this.speed, this.size);
	}

}
