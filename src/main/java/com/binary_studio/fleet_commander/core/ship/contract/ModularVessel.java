package com.binary_studio.fleet_commander.core.ship.contract;

import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

/**
 * Interface for ships which can be fitted at current moment.
 */
public interface ModularVessel {

	void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException;

	void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException;

}
