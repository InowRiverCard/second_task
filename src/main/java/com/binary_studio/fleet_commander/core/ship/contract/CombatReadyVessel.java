package com.binary_studio.fleet_commander.core.ship.contract;

import com.binary_studio.fleet_commander.core.common.CombatCapable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.TurnTrackingEntity;

/**
 * Interface for ships which can attack and be attacked.
 */
public interface CombatReadyVessel extends TurnTrackingEntity, NamedEntity, CombatCapable {

}
