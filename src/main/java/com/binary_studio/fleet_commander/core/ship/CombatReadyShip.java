package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;
import java.util.logging.Logger;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.util.ClassNameUtil;

/**
 * The class describes the ship which plows the vast expanses of space. It includes
 * functionality for fighting.
 *
 * @author Aleksandr Karpachov
 * @version 1.0
 */
public final class CombatReadyShip implements CombatReadyVessel {

	/**
	 * Logger
	 */
	private static final Logger LOG = Logger.getLogger(ClassNameUtil.getCurrentClassName());

	/**
	 * Systems name
	 */
	private String name;

	/**
	 * Max value of the ship shield <em>EHP</em>
	 */
	private final int shieldHPCap;

	/**
	 * Max value of the ship hull <em>EHP</em>
	 */
	private final int hullHPCap;

	/**
	 * Max value of the ship capacitor charge <em>sGA/H</em>.
	 */
	private final int capacitorCap;

	/**
	 * Current value of the ship shield <em>EHP</em>
	 */
	private PositiveInteger currentShieldHP;

	/**
	 * Current value of the ship hull <em>EHP</em>
	 */
	private PositiveInteger currentHullHP;

	/**
	 * Current value of the ship capacitor charge <em>sGA/H</em>.
	 */
	private PositiveInteger currentCapacitor;

	/**
	 * Amount of <em>sGA/H</em> which ship restore by each turn.
	 */
	private PositiveInteger capacitorRegeneration;

	/**
	 * The ship attack system.
	 */
	private AttackSubsystem attackSubsystem;

	/**
	 * The ship defencive system.
	 */
	private DefenciveSubsystem defenciveSubsystem;

	/**
	 * Current ship speed.
	 */
	private PositiveInteger speed;

	/**
	 * Ship size.
	 */
	private PositiveInteger size;

	CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHPCap = shieldHP.value();
		this.hullHPCap = hullHP.value();
		this.capacitorCap = capacitor.value();
		this.capacitorRegeneration = capacitorRegeneration;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.speed = speed;
		this.size = size;
		this.currentCapacitor = capacitor;
		this.currentShieldHP = shieldHP;
		this.currentHullHP = hullHP;
	}

	/**
	 * The method finishes turn and charges capacitor.
	 */
	@Override
	public void endTurn() {
		int updatedCapacitor = this.capacitorRegeneration.value() + this.currentCapacitor.value();
		this.currentCapacitor = PositiveInteger.of(Math.min(this.capacitorCap, updatedCapacitor));
		LOG.fine(String.format("Ship \"%s\" finished its turn", this.name));
	}

	/**
	 * The method starting turn.
	 */
	@Override
	public void startTurn() {
		LOG.fine(String.format("Ship \"%s\" begins its turn", this.name));
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	/**
	 * The method calculate damage through {@link AttackSubsystem#attack(Attackable)},
	 * creates {@link AttackAction} with obtained result and reduces charge level of the
	 * ship’s capacitor.
	 * @param target attacked object.
	 * @return {@link Optional<AttackAction>} with attack data. If ship have not enough
	 * capacitor charge level then returns empty Optional.
	 */
	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> result;
		LOG.fine(String.format("Ship \"%s\" attempts to attack ", target.getName()));
		if (this.currentCapacitor.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			result = Optional.empty();
			LOG.fine(String.format("Ship \"%s\". Not enough capacitor charge, current level=%d, necessary level=%d",
					this.name, this.currentCapacitor.value(), this.attackSubsystem.getCapacitorConsumption().value()));
		}
		else {
			var damage = this.attackSubsystem.attack(target);
			result = Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
			this.currentCapacitor = PositiveInteger
					.of(this.currentCapacitor.value() - this.attackSubsystem.getCapacitorConsumption().value());
			LOG.fine(String.format("Ship \"%s\" deals %d damage to target \"%s\"", this.name, damage.value(),
					target.getName()));
		}
		return result;
	}

	/**
	 * Method for handling incoming damage. At first reduce shield hp, then hull hp.
	 * @param attack {@link AttackAction} with attack data.
	 * @return {@link AttackResult.DamageRecived} with attack result data or
	 * {@link AttackResult.Destroyed} if shield and hull hp equals to zero
	 */
	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackResult result;
		LOG.fine(String.format(
				"Ship \"%s\" applies %d damage from attacker \"%s\" %n Before attack " + "shields=%s, hull=%s",
				this.name, attack.damage.value(), attack.attacker.getName(), this.currentShieldHP.value(),
				this.currentHullHP.value()));
		PositiveInteger damage = this.defenciveSubsystem.reduceDamage(attack).damage;
		int damageOverShield = this.breakShield(damage.value());
		boolean isDestroyed = this.breakHull(damageOverShield);
		if (isDestroyed) {
			result = new AttackResult.Destroyed();
			LOG.fine(String.format("Ship \"%s\". After attack destroyed :`(", this.name));
		}
		else {
			result = new AttackResult.DamageRecived(attack.weapon, damage, this);
			LOG.fine(String.format("Ship \"%s\". After attack shields=%s, hull=%s", this.name,
					this.currentShieldHP.value(), this.currentHullHP.value()));
		}
		return result;
	}

	/**
	 * The method calculate shield damage and sets new shield ehp value of this ship.
	 * @param damage income damage.
	 * @return damage that went through shields.
	 */
	private int breakShield(int damage) {
		int result;
		int afterAttackHP = this.currentShieldHP.value() - damage;
		if (afterAttackHP >= 0) {
			this.currentShieldHP = PositiveInteger.of(afterAttackHP);
			result = 0;
		}
		else {
			this.currentShieldHP = PositiveInteger.of(0);
			result = Math.abs(afterAttackHP);
		}
		return result;
	}

	/**
	 * The method calculate hull damage and sets new hull ehp value of this ship.
	 * @param damage income damage.
	 * @return {@code true} if this ship was defeated after attack or {@code false} if
	 * ship alive.
	 */
	private boolean breakHull(int damage) {
		boolean result;
		int afterAttackHP = this.currentHullHP.value() - damage;
		if (afterAttackHP > 0) {
			this.currentHullHP = PositiveInteger.of(afterAttackHP);
			result = false;
		}
		else {
			this.currentHullHP = PositiveInteger.of(0);
			result = true;
		}
		return result;
	}

	/**
	 * Regenerates hull hp and shield hp through this {@link DefenciveSubsystem}
	 * @return {@link Optional<RegenerateAction>} with regeneration data or empty
	 * {@link Optional} if capacitor charge level less then installed defencive system
	 * consumes.
	 */
	@Override
	public Optional<RegenerateAction> regenerate() {
		LOG.fine(String.format("Attempt to regenerate ship \"%s\" current shield=%d/%d, current hull = %d/%d",
				this.name, this.currentShieldHP.value(), this.shieldHPCap, this.currentHullHP.value(), this.hullHPCap));
		Optional<RegenerateAction> result;
		if (this.currentCapacitor.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			RegenerateAction regen = this.defenciveSubsystem.regenerate();
			PositiveInteger shieldRegen = this.regenerateShield(regen);
			PositiveInteger hullRegen = this.regenerateHull(regen);
			this.currentCapacitor = PositiveInteger
					.of(this.currentCapacitor.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			result = Optional.of(new RegenerateAction(shieldRegen, hullRegen));
		}
		else {
			result = Optional.empty();
			LOG.fine(
					String.format(
							"Not enough capacitorCap to regenerate ship \"%s\" current capacitorCap=%d,"
									+ "necessary=%d,",
							this.name, this.currentCapacitor.value(),
							this.defenciveSubsystem.getCapacitorConsumption().value()));
		}
		return result;
	}

	/**
	 * Support method for {@link this#regenerate()}. Regenerates hull hp.
	 * @param regen {@link RegenerateAction} which takes from current defencive system.
	 * @return amount of restored hull. If hull was full then returns zero.
	 */
	private PositiveInteger regenerateHull(RegenerateAction regen) {
		int hullShortage = this.hullHPCap - this.currentHullHP.value();
		PositiveInteger result;
		int updatedHull;
		if (hullShortage >= regen.hullHPRegenerated.value()) {
			updatedHull = this.currentHullHP.value() + regen.hullHPRegenerated.value();
			result = regen.hullHPRegenerated;
		}
		else {
			updatedHull = this.hullHPCap;
			result = PositiveInteger.of(hullShortage);
		}
		this.currentHullHP = PositiveInteger.of(updatedHull);
		LOG.fine(String.format("Ship \"%s\". Hull hp restored by %d units, current hull = %d/%d", this.name,
				result.value(), this.currentHullHP.value(), this.hullHPCap));
		return result;
	}

	/**
	 * Support method for {@link this#regenerate()}. Regenerates shield hp.
	 * @param regen {@link RegenerateAction} which takes from current defencive system.
	 * @return amount of restored shield. If hull was full then returns zero.
	 */
	private PositiveInteger regenerateShield(RegenerateAction regen) {
		int shieldShortage = this.shieldHPCap - this.currentShieldHP.value();
		PositiveInteger result;
		int updatedShield;
		if (shieldShortage >= regen.shieldHPRegenerated.value()) {
			updatedShield = this.currentShieldHP.value() + regen.shieldHPRegenerated.value();
			result = regen.shieldHPRegenerated;
		}
		else {
			updatedShield = this.shieldHPCap;
			result = PositiveInteger.of(shieldShortage);
		}
		this.currentShieldHP = PositiveInteger.of(updatedShield);
		LOG.fine(String.format("Ship \"%s\". Shield hp restored by %d units, current hull = %d/%d", this.name,
				result.value(), this.currentShieldHP.value(), this.shieldHPCap));
		return result;
	}

}
