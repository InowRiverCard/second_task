package com.binary_studio.fleet_commander.util;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 09.05.2020
 *
 * Class for working with logger. Thanks to it, the probability of substituting the wrong
 * class during copy-paste of the logger disappears.
 */
public final class ClassNameUtil {

	private ClassNameUtil() {
	}

	public static String getCurrentClassName() {
		try {
			throw new RuntimeException();
		}
		catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}

}
